import sys
import os
import pandas as pd
import commons


def transformCSVProm(csvPath): #cambiare nome colonne
  dataf = pd.read_csv(csvPath)
  dataOneHot=pd.get_dummies(dataf['event'], prefix='activity') #faccio un one hot encoding della colonna di activity
  dataWithoutTimeStamp=dataf[['case:concept:name','cluster','norm_TimeOfWeek']] #prendo queste 2 colonne
  max1=dataf['trace_time'].max() #trovo il valore massimo di queste 2 colonne
  max2=dataf['prev_event_time'].max()
  datanew1=dataf['trace_time'].truediv(max1) #normalizzo trace_time
  datanew2=dataf['prev_event_time'].truediv(max2) #normalizzo prev_event_time
  dataWithoutTimeStamp=pd.concat([dataWithoutTimeStamp,datanew1,datanew2,dataOneHot],axis=1) #riunisco tutto in un unico dataframe
  dataWithoutTimeStamp.to_csv(csvPath)


cluster = 0
dataframes = []

for elem in os.listdir('clusters/'):
  if elem.startswith("cl"):
    df = pd.read_csv('clusters/'+elem)
    df['cluster'] = str(cluster)
    cluster+=1
    dataframes.append(df)
dataframe = pd.concat(dataframes)
dataframe.to_csv("prom_clusters.csv")

transformCSVProm('prom_clusters.csv')
commons.evaluateClustering('prom_clusters.csv') 
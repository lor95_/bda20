import os
import pandas as pd
import numpy as np
from pm4py.objects.conversion.log import converter as log_converter
from pm4py.objects.log.adapters.pandas import csv_import_adapter
from pm4py.objects.conversion.log import factory as conversion_factory
from pm4py.objects.log.importer.xes import factory as xes_importer
from pm4pyclustering.algo.other.clustering import factory as clusterer
from pm4pyclustering.algo.other.conceptdrift.utils import get_representation
from sklearn.metrics import silhouette_score,silhouette_samples
from sklearn.metrics import pairwise_distances
from sklearn.metrics.pairwise import euclidean_distances
from sklearn.neighbors.nearest_centroid import NearestCentroid
from sklearn.preprocessing import OrdinalEncoder
from scipy.spatial import distance
import csv
from sklearn.preprocessing import OneHotEncoder
from numpy import var
from pm4py.objects.log.util import get_log_representation
from copy import copy
import commons

def traceClusteringcaseDuration(filePath,min=2,max=5):
  log = xes_importer.apply(filePath)
  clusters = clusterer.apply(log,parameters={"min_n_clusters_to_search":min, "max_n_clusters_to_search":max}, 
                             variant="duration")
  tot=0
  i=0
  df = pd.DataFrame(columns = ['concept:instance','concept:name','lifecycle:transition',
                             'time:timestamp','case:concept:name','cluster'])
  Trace=df['case:concept:name']
  Trace.to_csv('Trace.csv')
  for sublog in clusters:
      dataframe = log_converter.apply(sublog, variant=log_converter.Variants.TO_DATA_FRAME)
      dataframe['cluster'] = i
      df=df.append(dataframe)
      i=i+1
      tot=tot+len(sublog) 
      df.to_csv('clustersTime.csv') 
  print([len(x) for x in clusters])
  print(tot)

filePath='log.xes'
epsList=[0.5]
pca_componentsList=[1]
min=2
max=45
clusterPath1='clusters.csv'
clusterPath2='clustersTime.csv'
traceClusteringcaseDuration(filePath,min,max)
commons.transformCSV(clusterPath2)
commons.evaluateClustering(clusterPath2) 
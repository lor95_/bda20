import os
import pandas as pd
import numpy as np
from pm4py.objects.conversion.log import converter as log_converter
from pm4py.objects.log.adapters.pandas import csv_import_adapter
from pm4py.objects.conversion.log import factory as conversion_factory
from pm4py.objects.log.importer.xes import factory as xes_importer
from pm4pyclustering.algo.other.clustering import factory as clusterer
from pm4pyclustering.algo.other.conceptdrift.utils import get_representation
from sklearn.metrics import silhouette_score,silhouette_samples
from sklearn.metrics import pairwise_distances
from sklearn.metrics.pairwise import euclidean_distances
from sklearn.neighbors.nearest_centroid import NearestCentroid
from sklearn.preprocessing import OrdinalEncoder
from scipy.spatial import distance
import csv
from sklearn.preprocessing import OneHotEncoder
from numpy import var
from pm4py.objects.log.util import get_log_representation
from copy import copy


def evaluateClustering2(clusterPath): 
  data = pd.read_csv(clusterPath)
  nClusters=data['cluster'].nunique()#trovo il numero di cluster andando a vedendo gli elementi unici della colonna 'cluster'
  cluster_label=data['cluster'] #prendo solo la colonna con la label del cluster
  cluster_label=cluster_label.values #la trasformo in array (da dataframe)
  total=[]
  for i in range(nClusters): #per ogni cluster
    datacluster=data[data['cluster']==i] #faccio una selezione sulle righe (quindi prendo solo quelle del cluster iesimo)
    datacluster=datacluster.drop('cluster', axis=1) #drop della colonna con numero di cluster
    datac=datacluster.values #lo trasformo in un array
    total.append(np.mean(datac,axis=0)) #calcolo e appendo il centroide all'array total 
  total=np.delete(total, 0, 1) #elimino la prima colonna (index) 
  mediaDistanzeInter=[]
  for r in range(nClusters): #per ogni cluster
    tot=0
    for d in range(nClusters):
      tot=tot+distance.euclidean(total[r], total[d]) #calcolo la distanza di questo centroide con tutti gli altri e le sommo tra di loro
    if (nClusters>1):
      mediaDistanzeInter.append(tot/(nClusters-1)) #aggiungo alla lista la distanza media del centroide del cluster r
      #con i centroidi di tutti gli altri cluster
    else:
      mediaDistanzeInter.append(0) #la distanza intercluster è zero se c'è n'è uno solo
  sum=0
  for j in mediaDistanzeInter:
    sum=sum+j         
  meanDistanceInterClusters= sum/nClusters #calcolo la media tra tutte le distanze intercluster di tutti i centroidi
  print("meanDistanceInterClusters",meanDistanceInterClusters) 
  distances=[]
  meandistance=[]
  for k in range(nClusters):#per ogni cluster
    datacluster=data[data['cluster']==k] #faccio una selezione sulle righe
    datacluster=datacluster.drop('cluster', axis=1) #drop della colonna con numero di cluster
    datacs=datacluster.values #lo trasformo in un array
    datacs=np.delete(datacs, 0, 1) #elimino la prima colonna (index)  
    for elem in datacs: #per ogni riga del cluster
      distances.append(distance.euclidean(total[k], elem)) #calcolo la distanza tra quell'elemento e il suo centroide
      #e la appendo all'array distances
    meandistance.append(np.mean(distances,axis=0)) #calcolo la media delle distanze 
    distances=[]  
  sum=0
  for j in meandistance: 
    sum=sum+j 
  meanDistanceIntraClusters= sum/nClusters #calcolo la media tra tutte le distanze intracluster dei punti di ogni cluster
  #dai loro centroidi
  print("meanDistanceIntraClusters",meanDistanceIntraClusters)
  data=data.drop('cluster', axis=1) #drop della colonna con numero di cluster   
  dataf=data.values #lo trasformo in un array
  dataf=np.delete(dataf, 0, 1) #elimino la prima colonna (index)
  silhouette_avg=silhouette_score(dataf,cluster_label,metric='euclidean') #calcolo la silhouette media
  silhouettesamples=silhouette_samples(dataf,cluster_label)
  silhouette_var=var(silhouettesamples)
  print("sihouette_var",silhouette_var)
  print("silhouette avg",silhouette_avg)
  np.savetxt("silhouette_samples.txt",silhouettesamples,delimiter=';');
  f = open("results.txt", "a")
  f.write("mediaDistanzeInter="+str(meanDistanceInterClusters)+ 'meanDistanceIntraClusters='+str(meanDistanceIntraClusters)+
          ' silhouette='+str(silhouette_avg)+'\n')
  f.close()

  def evaluateClustering(clusterPath):
    filePath='log.xes'
    log = xes_importer.apply(filePath)
    parameters = {}
    log_list = []
    str_tr_attr = parameters["str_tr_attr"] if "str_tr_attr" in parameters else copy([])
    str_ev_attr = parameters["str_ev_attr"] if "str_ev_attr" in parameters else copy(["concept:name"])
    num_tr_attr = parameters["num_tr_attr"] if "num_tr_attr" in parameters else copy([])
    num_ev_attr = parameters["num_ev_attr"] if "num_ev_attr" in parameters else copy([])
    str_evsucc_attr = parameters["str_evsucc_attr"] if "str_evsucc_attr" in parameters else copy(["concept:name"])
    data, feature_names = get_log_representation.get_representation(log, str_tr_attr, str_ev_attr, num_tr_attr,
                                                                    num_ev_attr, str_evsucc_attr=str_evsucc_attr)
    data2=pd.read_csv(clusterPath) #dataframe con tutti gli eventi con relativo cluster
    data2=data2.sort_values(by=['case:concept:name'])#lo ordino (perchè è in ordine lessicografico cioè 1 100 101 etc)

    Ntraces=data2["case:concept:name"].unique() #numero di tracce
    traces,indices=np.unique(data2["case:concept:name"], return_index=True) #lista tracce ordinate e indice in cui si trovano
    clusterTraces2=[]
    traces2=sorted(traces,key=str) #ordino lessicograficamente le tracce perchè data è ordinato cosi
    for elem in traces2:
      datas=data2[data2["case:concept:name"]==elem] #prendo solo righe relative alla traccia "elem"
      datas=datas["cluster"] #mi prendo solo la colonna cluster
      clusterTraces2.append(datas.iloc[0]) #prendo il primo elemento perchè gli eventi di
      #una stessa traccia hanno tutti lo stesso cluster e lo appendo
    clusterTraces2=np.array(clusterTraces2) #trasformo in array
    clusterTraces=[]
    for elem in traces:
      datas=data2[data2["case:concept:name"]==elem] #prendo solo righe relative alla traccia "elem"
      datas=datas["cluster"] #mi prendo solo la colonna cluster
      clusterTraces.append(datas.iloc[0]) #prendo il primo elemento perchè hanno tutti stesso cluster e lo appendo
    clusterTraces=np.array(clusterTraces) #trasformo in array
    dff = pd.DataFrame({"NEvents":[], "Duration":[]}) #creo un dataframe vuoto
    thisset=set() #creo un insieme vuoto
    for elem in traces: #per ogni traccia
      if  elem not in thisset: #se non è presente nel set ce lo aggiungo
        thisset.add(elem)
        datar=data2[data2["case:concept:name"]==elem] #mi prendo solo le righe della traccia considerata
        durationLine=datar['trace_time'] #prendo solo la colonna con la durata della traccia
        duration=durationLine.max() #prendo la durata finale
        nactivities=datar.shape[0] #calcolo il numero di righe da cui è composta la traccia (cioè numero di eventi)
        dfr = pd.DataFrame({"NEvents":[nactivities], 
                    "Duration":[duration]}) 
        dff=dff.append(dfr,ignore_index=True) #lo aggiungo a dff
    dff=np.array(dff)#lo trasformo in array

    #metodo 1 utilizziamo due features per ogni traccia (durata e numero di eventi)
    #metodo 2 utilizziamo per ogni traccia tutte le features dateci dall'algoritmo di PM4Py

    silhouette_avg=silhouette_score(dff,clusterTraces,metric='euclidean') #calcolo la silhouette media del metodo 1
    silhouette_avg2=silhouette_score(data,clusterTraces2,metric='euclidean') #calcolo la silhouette media del metodo 2
    silhouettesamples1=silhouette_samples(dff,clusterTraces)
    silhouettesamples2=silhouette_samples(data,clusterTraces2)
    silhouette_var=var(silhouettesamples1) #calcolo la varianza della silhouette
    silhouette_var2=var(silhouettesamples2) #calcolo la varianza della silhouette
    print("sihouette_var metodo 1",silhouette_var)
    print("sihouette_var metodo 2",silhouette_var2)
    print("silhouette avg metodo 1",silhouette_avg)
    print("silhouette avg metodo 2",silhouette_avg2)

def transformCSV(csvPath):
  dataf = pd.read_csv(csvPath)
  dataStart=dataf[dataf['lifecycle:transition']=='start'] #selezione delle righe di start
  dataOneHot=pd.get_dummies(dataStart['concept:name'], prefix='activity') #faccio un dummy encoding della colonna di activity
  dataWithoutTimeStamp=dataStart[['case:concept:name','cluster','norm_TimeOfWeek']] #prendo queste 2 colonne
  max1=dataStart['trace_time'].max() #trovo il valore massimo di queste 2 colonne
  max2=dataStart['prev_event_time'].max()
  datanew1=dataStart['trace_time'].truediv(max1) #normalizzo trace_time
  datanew2=dataStart['prev_event_time'].truediv(max2) #normalizzo prev_event_time
  dataWithoutTimeStamp=pd.concat([dataWithoutTimeStamp,datanew1,datanew2,dataOneHot],axis=1) #riunisco tutto in un unico dataframe
  dataWithoutTimeStamp.to_csv(csvPath)